@Regression @Cart
Feature: Shopping cart tests

  Background: Be logged in
    Given Be on the login page
    And Put the username "standard_user"
    And Put the password "secret_sauce"
    And Click Login button
    And Products page is displayed

  Scenario: Add products to Shopping cart
    When Add first product to Shopping cart
    And Add second product to Shopping cart
    And Open Shopping cart page
    Then Shopping cart displays added products

  Scenario: Remove products from Shopping cart
    And Add first product to Shopping cart
    And Add second product to Shopping cart
    When Open Shopping cart page
    And Shopping cart displays added products
    And Remove first product from Shopping cart
    And Remove second product from Shopping cart
    Then Shopping cart is empty