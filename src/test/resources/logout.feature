@Regression @Logout
Feature: Logout tests

  Scenario: Logout user
    Given Be on the login page
    And Put the username "standard_user"
    And Put the password "secret_sauce"
    And Click Login button
    And Products page is displayed
    And Open main menu
    When Click Logout button
    Then Login button is displayed