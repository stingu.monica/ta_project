@Regression @Login
Feature: Login tests

  @Positive
  Scenario Outline: Login with valid credentials
    Given Be on the login page
    When Put the username <username>
    And Put the password <password>
    And Click Login button
    Then Products page is displayed
    Examples:
      | username        | password       |
      | "standard_user" | "secret_sauce" |

  @Negative
  Scenario Outline: Login with invalid credentials
    Given Be on the login page
    When Put the username <username>
    And Put the password <password>
    And Click Login button
    Then Error message is displayed
    Examples:
      | username        | password       |
      | "new_user"      | "secret_sauce" |
      | "standard_user" | "sauce_sauce"  |