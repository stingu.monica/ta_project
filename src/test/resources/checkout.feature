@Regression @Checkout
Feature: Checkout tests

  Background: Be logged in and add products to Shopping cart
    Given Be on the login page
    And Put the username "standard_user"
    And Put the password "secret_sauce"
    And Click Login button
    And Products page is displayed
    And Add first product to Shopping cart
    And Add second product to Shopping cart
    And Open Shopping cart page
    And Shopping cart displays added products

  @Positive
  Scenario: Checkout with required fields
    When Click Checkout button
    And Your Information page is displayed
    And Fill out required fields
      | Standard |
      | User     |
      | 123456   |
    And Click Continue button
    And Overview page is displayed
    And Click Finish button
    Then Checkout is done successfully

  @Negative
  Scenario: Checkout without required fields
    When Click Checkout button
    And Your Information page is displayed
    And Click Continue button
    Then Checkout error message is displayed