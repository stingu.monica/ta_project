package com.saucedemo.stepdefinitions;

import com.saucedemo.page_objects.Application;
import com.saucedemo.page_objects.HeaderPage;
import com.saucedemo.page_objects.LoginPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import static com.saucedemo.stepdefinitions.Hooks.driver;

public class LoginSteps {

    LoginPage login;

    @Given("Be on the login page")
    public void openLoginPage() {
        login = new Application(driver).navigateToLoginPage();
    }

    @When("Put the username {string}")
    public void putTheUsername(String username) {
        login.fillUsername(username);
    }

    @And("Put the password {string}")
    public void putThePassword(String password) {
        login.fillPassword(password);
    }

    @And("Click Login button")
    public void clickLoginButton() {
        login.submit();
    }

    @Then("Products page is displayed")
    public void productsPageIsDisplayed() {
        Assert.assertEquals("PRODUCTS", new HeaderPage(driver).getPageTitle());
    }

    @Then("Error message is displayed")
    public void errorMessageIsDisplayed() {
        Assert.assertEquals("Epic sadface: Username and password do not match any user in this service", login.getErrorMessage());
    }
}
