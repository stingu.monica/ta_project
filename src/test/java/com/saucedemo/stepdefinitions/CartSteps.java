package com.saucedemo.stepdefinitions;

import com.saucedemo.page_objects.CartPage;
import com.saucedemo.page_objects.HeaderPage;
import com.saucedemo.page_objects.ProductsPage;
import io.cucumber.java.en.When;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.Assert;

import static com.saucedemo.stepdefinitions.Hooks.driver;

public class CartSteps {

    CartPage cart = new CartPage(driver);
    ProductsPage products = new ProductsPage(driver);

    @When("Add first product to Shopping cart")
    public void addFirstProductToCart() {
        products.clickAddToCartBtn1();
    }


    @And("Add second product to Shopping cart")
    public void addSecondProductToCart() {
        products.clickAddToCartBtn2();
    }

    @And("Open Shopping cart page")
    public void openShoppingCartPage() {
        new HeaderPage(driver).openShoppingCart();
    }

    @Then("Shopping cart displays added products")
    public void shoppingCartDisplaysAddedProducts() {
        Assert.assertEquals("Sauce Labs Backpack", cart.getFirstProductLabel());
        Assert.assertEquals("Sauce Labs Bolt T-Shirt", cart.getSecondProductLabel());
    }

    @And("Remove first product from Shopping cart")
    public void removeFirstProductFromCart() {
        cart.clickRemoveBtn1();
    }

    @And("Remove second product from Shopping cart")
    public void removeSecondProductFromCart() {
        cart.clickRemoveBtn2();
    }

    @Then("Shopping cart is empty")
    public void shoppingCartIsEmpty() {
        Assert.assertTrue(cart.isCartEmpty());
    }
}
