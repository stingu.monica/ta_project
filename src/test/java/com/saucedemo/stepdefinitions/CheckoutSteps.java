package com.saucedemo.stepdefinitions;

import com.saucedemo.page_objects.CartPage;
import com.saucedemo.page_objects.CheckoutPage;
import com.saucedemo.page_objects.HeaderPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.util.List;

import static com.saucedemo.stepdefinitions.Hooks.driver;

public class CheckoutSteps {

    CheckoutPage checkout = new CheckoutPage(driver);
    HeaderPage header = new HeaderPage(driver);

    @When("Click Checkout button")
    public void clickCheckoutButton() {
        new CartPage(driver).clickCheckoutBtn();
    }

    @And("Your Information page is displayed")
    public void yourInformationPageIsDisplayed() {
        Assert.assertEquals("CHECKOUT: YOUR INFORMATION", header.getPageTitle());
    }

    @And("Fill out required fields")
    public void fillOutRequiredFields(List<String> checkoutFields) {
        checkout.fillFirstName(checkoutFields.get(0));
        checkout.fillLastName(checkoutFields.get(1));
        checkout.fillZipCode(checkoutFields.get(2));
    }

    @And("Click Continue button")
    public void clickContinueButton() {
        checkout.clickContinueBtn();
    }

    @And("Overview page is displayed")
    public void overviewPageIsDisplayed() {
        Assert.assertEquals("CHECKOUT: OVERVIEW", header.getPageTitle());
    }

    @And("Click Finish button")
    public void clickFinishButton() {
        checkout.clickFinishBtn();
    }

    @Then("Checkout is done successfully")
    public void checkoutIsDoneSuccessfully() {
        Assert.assertEquals("CHECKOUT: COMPLETE!", header.getPageTitle());
    }

    @Then("Checkout error message is displayed")
    public void checkoutErrorMessageIsDisplayed() {
        Assert.assertTrue(checkout.getErrorMessage().equals("Error: First Name is required"));
    }
}
