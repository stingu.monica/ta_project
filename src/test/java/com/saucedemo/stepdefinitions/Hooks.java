package com.saucedemo.stepdefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Hooks {

    protected static WebDriver driver;

    @Before
    public void setupBefore() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @After
    public void tearDownAfter() {
        driver.quit();
    }
}
