package com.saucedemo.stepdefinitions;

import com.saucedemo.page_objects.HeaderPage;
import com.saucedemo.page_objects.LoginPage;
import com.saucedemo.page_objects.MenuPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import static com.saucedemo.stepdefinitions.Hooks.driver;

public class LogoutSteps {

    MenuPage menu = new MenuPage(driver);

    @And("Open main menu")
    public void openMainMenu() {
        new HeaderPage(driver).openMenu();
    }

    @When("Click Logout button")
    public void clickLogoutButton() {
        menu.clickLogoutBtn();
    }

    @Then("Login button is displayed")
    public void loginButtonIsDisplayed() {
        Assert.assertTrue(new LoginPage(driver).isLoginBtnDisplayed());
    }
}
