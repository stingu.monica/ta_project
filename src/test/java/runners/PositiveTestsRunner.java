package runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        dryRun = false,
        features = "src/test/resources",
        glue = "com/saucedemo/stepdefinitions",
        tags = "@Positive",
        plugin = {"pretty", "html:target/PositiveTests.html"}
)

public class PositiveTestsRunner {
}
