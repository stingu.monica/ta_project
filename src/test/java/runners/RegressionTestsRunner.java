package runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        dryRun = true,
        features = "src/test/resources",
        glue = "com/saucedemo/stepdefinitions",
        tags = "@Regression",
        plugin = {"pretty", "html:target/RegressionTests.html"}
)

public class RegressionTestsRunner {
}
