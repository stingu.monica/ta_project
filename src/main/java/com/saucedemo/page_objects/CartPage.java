package com.saucedemo.page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class CartPage {

    private WebDriver driver;
    private By firstProductLabel = By.id("item_4_title_link");
    private By secondProductLabel = By.id("item_1_title_link");
    private By removeBtn1 = By.id("remove-sauce-labs-backpack");
    private By removeBtn2 = By.id("remove-sauce-labs-bolt-t-shirt");
    private By emptyCart = By.className("removed_cart_item");
    private By checkoutBtn = By.id("checkout");

    public CartPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getFirstProductLabel() {
        return driver.findElement(firstProductLabel).getText();
    }

    public String getSecondProductLabel() {
        return driver.findElement(secondProductLabel).getText();
    }

    public void clickRemoveBtn1() {
        driver.findElement(removeBtn1).click();
    }

    public void clickRemoveBtn2() {
        driver.findElement(removeBtn2).click();
    }

    public boolean isCartEmpty() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.presenceOfElementLocated((emptyCart)));
        return driver.findElement(emptyCart).isEnabled();
    }

    public void clickCheckoutBtn() {
        driver.findElement(checkoutBtn).click();
    }
}
