package com.saucedemo.page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class HeaderPage {

    private WebDriver driver;
    private By menuBtn = By.className("bm-burger-button");
    private By shoppingCartBtn = By.className("shopping_cart_link");
    private By pageTitle = By.className("title");

    public HeaderPage(WebDriver driver) {
        this.driver = driver;
    }

    public void openMenu() {
        driver.findElement(menuBtn).click();
    }

    public void openShoppingCart() {
        driver.findElement(shoppingCartBtn).click();
    }

    public String getPageTitle() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(pageTitle)));
        return driver.findElement(pageTitle).getText();
    }
}
