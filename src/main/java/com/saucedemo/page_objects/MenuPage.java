package com.saucedemo.page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class MenuPage {

    private WebDriver driver;
    private By logoutBtn = By.id("logout_sidebar_link");

    public MenuPage(WebDriver driver) {
        this.driver = driver;
    }

    public void clickLogoutBtn() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(logoutBtn)));
        driver.findElement(logoutBtn).click();
    }
}
