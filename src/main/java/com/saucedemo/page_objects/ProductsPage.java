package com.saucedemo.page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ProductsPage {

    private WebDriver driver;
    private By addToCartBtn1 = By.id("add-to-cart-sauce-labs-backpack");
    private By addToCartBtn2 = By.id("add-to-cart-sauce-labs-bolt-t-shirt");

    public ProductsPage(WebDriver driver) {
        this.driver = driver;
    }

    public void clickAddToCartBtn1() {
        driver.findElement(addToCartBtn1).click();
    }

    public void clickAddToCartBtn2() {
        driver.findElement(addToCartBtn2).click();
    }

}
